
const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema(
	{
	orderList: [
		{
			productId: {
				type: String,
				required: [true, "ProductId is required."]
			},
			name: {
				type: String,
				required: [true, "Name is required."]
			},
			description: {
				type: String,
				required: [true, "Description is required."]
			},
			price : {
				type: Number,
				required: [true, "Price is required."]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}	
	]
});

module.exports = mongoose.model ("Order", orderSchema);