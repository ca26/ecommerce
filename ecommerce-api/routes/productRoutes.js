
const express = require("express");
const router = express.Router();
let auth = require("./../auth");
//controllers
const productController = require('./../controllers/productControllers');

router.get('/active', (req, res) => {

	productController.getAllActive().then( result => res.send(result));
})

router.get("/all", auth.verify, (req, res) => {

	productController.getAllProducts().then( result => res.send(result))
})

router.post('/addProduct', auth.verify, (req, res) => {

	productController.addProduct(req.body).then( result => res.send(result))
})

router.get('/:productId', auth.verify, (req, res) => {

	productController.getProduct(req.params).then( result => res.send(result))
})

router.put('/:productId/edit', auth.verify, (req, res) => {
	console.log(req.params.productId)

	productController.editProduct(req.params.productId, req.body).then( result => res.send(result))
})

router.put('/:productId/archive', auth.verify, (req, res) => {

	productController.archiveProduct(req.params.productId).then( result => res.send(result))
})

module.exports = router;
