
const express = require("express");
const router = express.Router();
let auth = require("./../auth");
const orderController = require("./../controllers/orderControllers");

router.post('/addCart', auth.verify, (req, res) => {

	orderController.addCart(req.body).then( result => res.send(result))
})

module.exports = router;