
const express = require("express");
const router = express.Router();
const userController = require("./../controllers/userControllers");
const auth = require("./../auth");

router.post("/checkEmail", (req, res) => {
	// console.log(req.body)
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

router.post("/register", (req, res)=> {
	userController.register(req.body).then(result => res.send(result));
})

router.post("/login", (req, res) => {

	userController.login(req.body).then( result => res.send(result))
})

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	// console.log(userData)

	userController.getProfile(userData.id).then(result => res.send(result))
})

router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {
	console.log(req.params.userId)

	userController.editUser(req.params.userId).then( result => res.send(result))
})

router.post('/addCart', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	userController.addCart(data).then( result => res.send(result))
})

module.exports = router;
